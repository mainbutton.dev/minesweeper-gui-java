package cja.su.minesweeper.main;

import javax.swing.*;
import java.awt.event.ActionListener;

public class Cell extends JButton {

    private final int MINE = 10;
    public Cell[][] cells;

    private final int row;
    private final int col;
    private       int value;
    private       int gridSize = Minesweeper.gridSize;

    Cell(final int row, final int col, final ActionListener actionListener) {
        this.row = row;
        this.col = col;
        this.addActionListener(actionListener);
        setText("");
    }

    public int getMINE() {
        return MINE;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isMine() {
        return value == MINE;
    }

    public void reveal() {
        setEnabled(false);
        setText(isMine() ? "*" : (value == 0 ? " " : String.valueOf(value)));
    }

    public Cell[] getNeighbours() {

        Cell[] neighbours = new Cell[8];

        int index = 0;

        for (int rowOffset = -1; rowOffset <= 1; rowOffset ++) {
            for (int colOffset = -1; colOffset <= 1; colOffset ++) {
                // Skip ourselves
                if(rowOffset == 0 && colOffset == 0) continue;
                
                int rowValue = row + rowOffset;
                int colValue = col + colOffset;

                // Make sure that doesnt picked a cell expect the grid
                if (rowValue < 0 || rowValue >= gridSize
                    || colValue < 0 || colValue >= gridSize) {
                    continue;
                }

                neighbours[index] = Minesweeper.cells[rowValue][colValue];

                index ++;
            }
        }

        return neighbours;
    }
}
