package cja.su.minesweeper.main;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
/* import java.util.HashSet; */
import java.util.Random;
/* import java.util.Set; */

public class Minesweeper {

    // Enables to show debugs messages
    public static boolean debug = false;

    // just (a) random
    Random ran = new Random();

    // The value of the mines
    private static final int MINE = 10;

    // Pixel size of JFrame
    private static final int SIZE = 500;

    // The number of mines in the grid size
    public static final double POPULATION_CONSTANT =  1.5;

    // The number of the cells in one row and column
    public static int gridSize;

    private JFrame  frame;
    private JButton reset;
    private JButton giveUp;
    private JButton exit;

    public static Cell[][] cells = new Cell[gridSize][gridSize];

    // new arrays every time a cell's neighbours are to be retrieved.
    private static Cell[] reusableNeighbourCells = new Cell[8];

    // Saves the action what will happen if a button is clicked
    private final ActionListener actionListener = actionEvent -> {
        Object target = actionEvent.getSource();

        if (target == reset) {
            reset();
            createMines();
        } else if (target == giveUp) {

            Main.openDialog(frame, "Du hast aufgegeben!", "Game over!");
            reset();

        } else if (target == exit) {

            int exitDialog = JOptionPane.showConfirmDialog(null, "Wollen Sie wirklich beenden?",
                    "Wirklich beenden?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if(exitDialog == JOptionPane.YES_OPTION) {
                System.exit(100);
            }
        } else {
            handleCell((Cell) target);
        }
    };
    // run if you clicked on a field
    private void handleCell(Cell cell) {

        if (cell.isMine()) {
            cell.setBackground(Color.RED);
            cell.reveal();

            Main.openDialog(frame, "Du hast verloren!" , "Game over!");

            createMines();
        }
        else if(cell.getValue() == 0) cascade(cell);
        else cell.reveal();

        checkForWin();
    }

    // * Minesweeper main method
    private Minesweeper(final int gridSize) {
        this.gridSize = gridSize;
        cells         = new Cell[gridSize][gridSize];
        frame         = new JFrame(" - MEIN SWIEPER © BY DIETER THE PIEPER - xD");

        // field is a square
        frame.setSize(SIZE, SIZE);
        frame.setLayout(new BorderLayout());

        initializeButtonPanel(); // Buttons: Reset, Give Up, Exit (specially for Mr. Lange)
        initializeGrid();       // create grid container with fields

        frame.setLocationRelativeTo(null);  // set win in the middle
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   // min, max, close
        frame.setVisible(true); // run window
    }

    // * create Buttons at bottom of frame
    private void initializeButtonPanel() {
        JPanel buttonPanel = new JPanel();

        reset  = new JButton("Reset");
        giveUp = new JButton("Give Up");
        exit   = new JButton("Exit");

        // add actionListener to every Button
        reset .addActionListener(actionListener);
        giveUp.addActionListener(actionListener);
        exit  .addActionListener(actionListener);

        buttonPanel.add(reset);
        buttonPanel.add(giveUp);
        buttonPanel.add(exit);
        // set Buttons in the bottom of the frame
        frame      .add(buttonPanel, BorderLayout.SOUTH);
    }

    // * create field with clickable buttons
    private void initializeGrid() {
        Container grid = new Container();
        grid.setLayout(new GridLayout(gridSize, gridSize)); // create grid layout with buttons

        // fill cells[][] with cell instances
        for (int row = 0; row < gridSize; row++) {
            for (int col = 0; col < gridSize; col++) {
                cells[row][col] = new Cell(row, col, actionListener);
                grid.add(cells[row][col]);
            }
        }
        createMines();  // add mines to cells[][] randomly
        frame.add(grid, BorderLayout.CENTER);   // add cells[][] to frame
    }

    // * reset
    private void reset() {
        // set all cells to default values
        for(int row = 0; row < gridSize; row++) {
            for(int col = 0; col < gridSize; col++) {
                cells[row][col].setEnabled(true);
                cells[row][col].setText("");
                cells[row][col].setBackground(null);
                cells[row][col].setForeground(null);
                cells[row][col].setValue(0);
            }
        }
    }

    // * create Mines
    private void createMines() {
        if(debug) System.out.println("Create Meins!");

        reset();

        // count of Mines: gridSize(10) * Const(1.5) = 15 Mines 
        for(int i = 0; i < gridSize * POPULATION_CONSTANT; i++ ){

            // pos of current mine
            int x;
            int y;

            do {    // repeat until mine is set on a free cell

                // create for x and y random values within the gridSize
                x = ran.nextInt(gridSize);                
                y = ran.nextInt(gridSize);

                cells[x][y].setValue(MINE); // set value of cell to mine

                if(debug) cells[x][y].setBackground(Color.RED);
                if(debug) System.out.println("x: " + x + "; y: " + y);
            }
            while(cells[x][y].getValue() != MINE);
        }

        indexValues();
    }

    private void indexValues() {
        for(int row = 0; row < gridSize; row++) {     // Check every cell in
            for(int col = 0; col < gridSize; col++) { // every row and column

                Cell cell = cells[row][col];

                // check if the cell is a mine
                if(cell.isMine()) {
                    continue;
                }

                // check how much mines are in the neighbourhood
                for (int rowOffset = -1; rowOffset <= 1; rowOffset++) {
                    for (int colOffset = -1; colOffset <= 1; colOffset++) {

                        // Skip ourselves
                        if (rowOffset == 0 && colOffset == 0) {
                            continue;
                        }

                        int rowValue = row + rowOffset; // get absolute position
                        int colValue = col + colOffset; // by relative position

                        if (rowValue < 0 || rowValue > gridSize - 1           // check if the cell is
                                || colValue < 0 || colValue > gridSize - 1) { // inside the gird
                            continue;
                        }
                        // check if the neighbour cell is a mine
                        if (cells[rowValue][colValue].isMine())
                            cells[row][col].setValue(cells[row][col].getValue() + 1);

                        if (debug) cell.setEnabled(true);
                        if (debug) cell.setText("" + cells[row][col].getValue());
                    }
                }
            }
        }
    }

    // * check for win
    private void checkForWin() {
        boolean won = true;

        rows:   // check if a mine is clicked
        for (Cell[] cellRow : cells) {
            for (Cell cell : cellRow) {
                if (!cell.isMine() && cell.isEnabled()) {
                    won = false;
                    break rows;
                }
            }
        }

        if (won) {  // Dialog if won
            JOptionPane.showMessageDialog(frame, "Du hast gewonnen!", "Minesweeper",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    // * check neighbourhood of current cell
    private void cascade(Cell target) {

        // Get neighbourhood
        reusableNeighbourCells = target.getNeighbours();

        target.reveal();

        // if the cell hasn't a mine in his hood,
        // than checking his hood
        if (target.getValue() == 0) {
            for (Cell cell : reusableNeighbourCells) {
                if(cell != null) { // only necessary for bordered cells, because there can be saved cells out of the grid
                    if (!(cell.getRow() < 0 || cell.getRow() > gridSize - 1       // if the gird contains
                        || cell.getCol() < 0 || cell.getCol() > gridSize - 1)) {  // the cell

                        if (cell.isEnabled()) // Check if the cells is already revealed
                            cascade(cell);
                    }
                }
            }
        }
    }

    // * run method
    static void run(final int gridSize) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {

        }
        new Minesweeper(gridSize);
    }
}
